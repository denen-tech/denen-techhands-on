<?php
/**
 * Q1.数字の入力のみを許容したい。$patternに適切な正規表現を定義してテストケースをチェックしてください。
 * expectedがinputを許容するかどうかを表しています。
 * 
 * PHPファイルの実行方法は以下の通りです。
 * > php {file.php}
 * 
 * ※答えは数パターンあります。
 */

$pattern = '/.*/';

// テストケース
$test_cases = array(
    array (
        'input' => '101回目の呪い',
        'expected' => false,
    ),
    array (
        'input' => '万の夜をこえて',
        'expected' => false,
    ),
    array (
        'input' => 'SAY NO',
        'expected' => false,
    ),
    array (
        'input' => '４６４５',
        'expected' => false,
    ),
    array (
        'input' => '22',
        'expected' => true,
    )
);

// テストケースをループ
foreach ($test_cases as $index => $test_case) {
    $is_matched = preg_match($pattern, $test_case['input']);

    if ($test_case['expected'] == $is_matched) {
        echo '['. ($index + 1) . ']: 期待通りの結果が得られています。' . PHP_EOL;
    } else {
        echo '['. ($index + 1) . ']: 期待と異なるチェック結果です。' . PHP_EOL;
    }
}
