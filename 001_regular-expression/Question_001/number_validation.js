/**
 * Q1.数字の入力のみを許容したい。$patternに適切な正規表現を定義してテストケースをチェックしてください。
 * expectedがinputを許容するかどうかを表しています。
 *
 * JavaScriptファイルの実行方法は以下の通りです。
 * > node {file.js}
 *
 * ※答えは数パターンあります。
 */

const pattern = /.*/;

// テストケース
const test_cases = [
    {
        input: '101回目の呪い',
        expected: false,
    },
    {
        input: '万の夜をこえて',
        expected: false,
    },
    {
        input: 'SAY NO',
        expected: false,
    },
    {
        input: '４６４５',
        expected: false,
    },
    {
        input: '22',
        expected: true,
    },
];

// テストケースをループ
test_cases.forEach((test_case, index) => {
    const is_matched = pattern.test(test_case.input);

    if (test_case.expected === is_matched) {
        console.log(`[${index + 1}]: 期待通りの結果が得られています。`);
    } else {
        console.log(`[${index + 1}]: 期待と異なるチェック結果です。`);
    }
});

